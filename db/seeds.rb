@items = 40
@items.times do |index|

    Book.create({
        id: index,
        title: Faker::Book.title,
        description: Faker::Books::Lovecraft.paragraph_by_chars(characters: 128),
        isbn: Faker::IDNumber.valid,
        status: 1,
        publisher_id: rand(1..@items)
    })

    Author.create({
        id: index,
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        email: Faker::Internet.email,
        birthday: Faker::Date.birthday(min_age: 30, max_age: 120),
        status: 1
    })

    Publisher.create({
        id: index,
        name: Faker::FunnyName.name,
        phone: Faker::PhoneNumber.cell_phone_with_country_code,
        address: Faker::Address.street_name + ", " + Faker::Address.street_address + ", " + Faker::Address.country,
    })

    BookAuthor.create({
        book_id: rand(1..@items),
        author_id: rand(1..@items)
    })

    PublisherAuthor.create({
        publisher_id: rand(1..@items),
        author_id: rand(1..@items)
    })
    
end
