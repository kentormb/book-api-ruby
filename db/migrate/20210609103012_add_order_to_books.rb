class AddOrderToBooks < ActiveRecord::Migration[6.1]
  def change
    add_column :books, :order, :string
  end
end
