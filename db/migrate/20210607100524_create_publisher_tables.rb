class CreatePublisherTables < ActiveRecord::Migration[6.1]
  def change
    create_table :publisher_tables do |t|
      t.integer :author_id
      t.integer :publisher_id

      t.timestamps
    end
  end
end
