class AddDescriptionToBooks < ActiveRecord::Migration[6.1]
  def change
    add_column :books, :description, :text
    add_column :books, :publisher_id, :integer
    add_column :books, :status, :integer
  end
end
