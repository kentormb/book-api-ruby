class CreatePublishers < ActiveRecord::Migration[6.1]
  def change
    create_table :publishers do |t|
      t.string :name
      t.string :phone
      t.string :address
      t.string :status
      t.string :integer

      t.timestamps
    end
  end
end
