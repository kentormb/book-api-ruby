module Api
    module V1
        class PublishersController < ApplicationController

            def index
                publishers = Publisher.order('created_at DESC')
                render json: {status: 'SUCCESS', data: publishers }, status: :ok
            end

            def index_orderd
                books = Publisher.new.get_books_by_publisher(params[:id], params[:orderby])
                render json: {status: 'SUCCESS', data: books }, status: :ok
            end

            def show
                publisher = Publisher.find(params[:id])
                render json: {status: "SUCCESS", data: publisher}, status: :ok
            end

            def create
                publisher = Publisher.new(publisher_params)
                if publisher.save
                    render json: {status: "SUCCESS", data: publisher}, status: :created
                else
                    render json: {status: "ERROR", data: publisher.errors}, status: :unprocessable_entity
                end
            end

            def destroy
                publisher = Publisher.find(params[:id])
                publisher.destroy
                render json: {status: "SUCCESS", data: publisher}, status: :ok
            end

            def update
                publisher = Publisher.find(params[:id])
                if Publisher.update(publisher_params)
                    render json: {status: "SUCCESS", data: publisher}
                else
                    render json: {status: "ERROR", data: publisher.errors}
                end
            end
        end
    end
end