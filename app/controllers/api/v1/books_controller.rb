module Api
    module V1
        class BooksController < ApplicationController
            
            def index
                books = Book.new.get_all_books
                render json: {status: 'SUCCESS', data: books }, status: :ok
            end

            def index_with_pagination
                books = Book.new.get_all_books_with_pagination(params)
                render json: {status: 'SUCCESS', data: books }, status: :ok
            end

            def index_by_publisher
                books = Book.new.get_books_by_publisher(params[:id])
                render json: {status: 'SUCCESS', data: books }, status: :ok
            end

            def show
                book = Book.new.get_book(params[:id])
                render json: {status: "SUCCESS", data: book}, status: :ok
            end

            def create
                book = Book.new(book_params)
                if book.save
                    render json: {status: "SUCCESS", data: book}, status: :ok
                else
                    render json: {status: "ERROR", data: book.errors}, status: :unprocessable_entity
                end
            end

            def destroy
                book = Book.find(params[:id])
                book.destroy
                render json: {status: "SUCCESS", data: book}, status: :ok
            end

            def update
                book = Book.find(params[:id])
                if book.update(book_params)
                    render json: {status: "SUCCESS", data: book}, status: :ok
                else
                    render json: {status: "ERROR", data: book.errors}, status: :unprocessable_entity
                end
            end
        end
    end
end