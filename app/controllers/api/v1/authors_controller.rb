module Api
    module V1
        class AuthorsController < ApplicationController
            
            def index
                authors = Author.order('created_at DESC')
                render json: {status: 'SUCCESS', data: authors }, status: :ok
            end

            def show
                author = Author.find(params[:id])
                render json: {status: "SUCCESS", data: author}, status: :ok
            end

            def create
                author = Author.new(author_params)
                if author.save
                    render json: {status: "SUCCESS", data: author}, status: :ok
                else
                    render json: {status: "ERROR", data: author.errors}, status: :unprocessable_entity
                end
            end

            def destroy
                author = Author.find(params[:id])
                author.destroy
                render json: {status: "SUCCESS", data: author}, status: :ok
            end

            def update
                author = Author.find(params[:id])
                if Author.update(author_params)
                    render json: {status: "SUCCESS", data: author}, status: :ok
                else
                    render json: {status: "ERROR", data: author.errors}, status: :unprocessable_entity
                end
            end
            
        end
    end
end