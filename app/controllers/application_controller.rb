class ApplicationController < ActionController::API

    def book_params
        params.permit(:title, :description, :isbn, :status, :publisher_id, :order)
    end

    def author_params
        params.permit(:first_name, :last_name, :email, :birthday, :status)
    end

    def publisher_params
        params.permit(:name, :phone, :address, :status)
    end

end
