class Book < ApplicationRecord
    validates :title, presence: true, length: {minimum:1}
    validates :isbn, presence: true, length: {minimum:10}
    validates :description, length: {maximum:512}

    has_many :book_authors
    has_many :authors, through: :book_authors
    belongs_to :publisher

    def get_all_books
        #sql = "SELECT title, description, isbn, last_name || ' ' || first_name as author_name FROM 
        #`books` LEFT JOIN book_authors ON books.id = book_authors.book_id 
        #LEFT JOIN `authors` ON `authors`.id = book_authors.author_id 
        #WHERE authors.email IS NOT NULL 
        #GROUP BY books.id 
        #ORDER BY last_name, books.`order`"
        #books = ActiveRecord::Base.connection.exec_query(sql)
        books = Book.left_outer_joins(:authors)
                    .where("authors.email IS NOT NULL")
                    .select("books.id, title, description, isbn, last_name || ' ' || first_name as author_name")
                    .group("books.id")
                    .order(:last_name, :order)
        books.each do |b|
            b['description'] = truncate_description(b['description'])
        end
    end

    def get_all_books_with_pagination(params)
        limit = params[:limit] ? params[:limit].to_i : 10
        page = params[:page] ? params[:page].to_i - 1 : 0
        offset = limit * page

        books = Book.left_outer_joins(:authors)
                    .where("authors.email IS NOT NULL")
                    .select("books.id, title, description, isbn, last_name || ' ' || first_name as author_name")
                    .group("books.id")
                    .order(:last_name, :order)
                    .limit(limit)
                    .offset(offset)
        books.each do |b|
            b['description'] = truncate_description(b['description'])
        end
    end

    def get_books_by_publisher(publisher_id)
        #sql = "SELECT title, description, isbn, last_name || ' ' || first_name as author_name FROM 
        #`books` LEFT JOIN book_authors ON books.id = book_authors.book_id 
        #LEFT JOIN `authors` ON `authors`.id = book_authors.author_id 
        #LEFT JOIN publishers on books.publisher_id = publishers.id 
        #WHERE authors.email IS NOT NULL AND books.publisher_id = #{params[:id]} 
        #GROUP BY books.id 
        #ORDER BY last_name, books.`order`"
        #books = ActiveRecord::Base.connection.exec_query(sql)
        books = Book.left_outer_joins(:authors, :publisher)
                    .where("authors.email IS NOT NULL")
                    .where("books.publisher_id = ?", publisher_id)
                    .select("books.id, title, description, isbn, last_name || ' ' || first_name as author_name")
                    .group("last_name, books.`order`")
                    .order(:last_name, :order)
                    .uniq
        books.each do |b|
            b['description'] = truncate_description(b['description'])
        end
    end

    def get_book(id)
        # sql = "SELECT title, description, isbn, books.created_at, last_name || ' ' || first_name as author_name, authors.email, authors.birthday, publishers.name as publisher_name, publishers.address as publisher_address FROM `books` LEFT JOIN book_authors ON books.id = book_authors.book_id LEFT JOIN `authors` ON `authors`.id = book_authors.author_id LEFT JOIN publishers on books.publisher_id = publishers.id WHERE books.id = #{params[:id]}"
        # book = ActiveRecord::Base.connection.exec_query(sql)
        # if book.length > 0 
        #     book.first['authors'] = []
        #     book.each do |b|
        #         b['description'] = b['description'].truncate(100)
        #         b['created_at'] = (Date.parse b['created_at']).strftime("%d/%m/%Y")
        #         if b['email']
        #             date = Date.parse b['birthday']
        #             author = {name: b['author_name'], email: b['email'], birthday: date.strftime("#{date.day.ordinalize} of %B %Y") }
        #             b.delete("author_name")
        #             b.delete("email")
        #             b.delete("birthday")
        #             book.first['authors'].push(author)
        #         end
        #     end
        # end    
        book = Book.left_outer_joins(:authors, :publisher)
                    .where('books.id = ?', id)
                    .select("books.id, title, description, isbn, books.created_at, last_name || ' ' || first_name as author_name, authors.email, authors.birthday, publishers.name as publisher_name, publishers.address as publisher_address")
        item = {}         
        if book.length > 0 
            item['authors'] = []
            book.each do |b|
                item['id'] = b['id']
                item['description'] = truncate_description(b['description'])
                item['created_at'] = (Date.parse b['created_at'].to_s).strftime("%d/%m/%Y")
                item['title'] = b['title']
                item['isbn'] = b['isbn']
                item['publisher_name'] = b['publisher_name']
                item['publisher_address'] = b['publisher_address']
                if b['email']
                    date = Date.parse b['birthday']
                    author = {name: b['author_name'], email: b['email'], birthday: date.strftime("#{date.day.ordinalize} of %B %Y") }
                    item['authors'].push(author)
                end
            end
        end
        return item
    end
end
