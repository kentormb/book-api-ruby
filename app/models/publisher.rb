class Publisher < ApplicationRecord
    validates :name, presence: true, length: {minimum:1}
    validates :phone, presence: true, length: {minimum:1}
    validates :address, presence: true, length: {minimum:1}

    has_many :publisher_authors
    has_many :authors, through: :publisher_authors
    has_many :books

    def get_books_by_publisher(publisher_id, orderby)
        #sql = "SELECT title, description, isbn, last_name, first_name 
        #FROM `books` LEFT JOIN book_authors ON books.id = book_authors.book_id 
        #LEFT JOIN `authors` ON `authors`.id = book_authors.author_id 
        #WHERE publisher_id = #{params[:id]}"
        # if params[:orderby] == 'book'
        #     sql += ' ORDER BY last_name'
        # else
        #     sql += ' ORDER BY last_name, books.id DESC'
        # end
        # publishers = ActiveRecord::Base.connection.exec_query(sql)
        books = Book.left_outer_joins(:authors)
                    .where("publisher_id = ?", publisher_id)
                    .select("books.id, title, description, isbn, last_name, first_name ")
                    .group('books.id')
        if orderby == 'book'
            books.order(:last_name)
        else
            books.order(:last_name, 'books.id DESC')
        end            
        return books
    end
end
