class Author < ApplicationRecord
    validates :first_name, presence: true, length: {minimum:1}
    validates :last_name, presence: true, length: {minimum:1}
    validates :email, presence: true, length: {minimum:1}

    has_many :publisher_authors
    has_many :authors, through: :publisher_authors
    has_many :book_authors
    has_many :books, through: :book_authors
end
