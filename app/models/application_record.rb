class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def truncate_description(description, length = 100)
    description.truncate(length)
  end
end
