Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      # get '/books', to: 'books#index'
      # get '/books/:id', to: 'books#show'
      # post '/books', to: 'books#create'
      # put '/books/:id', to: 'books#update'
      # delete '/books/:id', to: 'books#destroy'
      resources :books
      get '/books/publisher/:id', to: 'books#index_by_publisher'
      get '/books/page/:page(/limit/:limit)', to: 'books#index_with_pagination'

      # get '/publishers', to: 'publishers#index'
      # get '/publishers/:id', to: 'publishers#show'
      # post '/publishers', to: 'publishers#create'
      # put '/publishers/:id', to: 'publishers#update'
      # delete '/publishers/:id', to: 'publishers#destroy'
      get '/publishers/:id/orderby/:orderby', to: 'publishers#index_orderd'
      resources :publishers

      # get '/authors', to: 'authors#index'
      # get '/authors/:id', to: 'authors#show'
      # post '/authors', to: 'authors#create'
      # put '/authors/:id', to: 'authors#update'
      # delete '/authors/:id', to: 'authors#destroy'
      resources :authors
    end
  end
end
